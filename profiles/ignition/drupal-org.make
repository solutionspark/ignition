api = 2

core = 7.24

projects[admin_menu] = 3.0-rc4
projects[ckeditor] = 1.13
projects[ctools] = 1.3
projects[devel] = 1.3
projects[google_analytics] = 1.4
projects[libraries] = 2.1
projects[metatag] = 1.0-beta7
projects[module_filter] = 1.8
projects[pathauto] = 1.2
projects[token] = 1.5
projects[views] = 3.7

libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.2.2/ckeditor_4.2.2_full.tar.gz"
