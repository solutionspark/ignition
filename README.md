## _Ignition_ Drupal 7 starter profile

Includes everything you need for a successful jump start.

UI improvements.

All essential modules.

### Dev tools
* Devel
* Registry rebuild

## Contrib
* admin_menu (enabled)
* ckeditor
* diff
* features
* google_analytics
* metatag
* pathauto
* strongarm
* views
* block_class
* ctools
* empty_page
* fences
* libraries
* module_filter
* semanticviews
* token
* workbench
* menu_editor
* backup_migrate
* smart_trim
* r4032login

## Initial Drupal configuration
* Creates an empty page as the front page
* Adds and configures Full HTML text format
* Configures CKEditor to work with Full HTML fomat, adds buttons
* Sets the default "Basic page" content type not to be promoted and have comments disabled. Disables date and author information.
* Allows visitor account creation, but with administrative approval.
* Enables default permissions for system roles.
* Creates a Home link in the main menu.
* Enables the admin theme.


